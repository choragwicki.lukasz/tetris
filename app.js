import './extensions/mathExtensions.js';

const container = document.querySelector('.container');
const btn = document.querySelector('.play-btn');
const play = document.querySelector('.play');
const rowsCount = 22;
const colsCount = 10;
const firstShape = `<div class="figure figure--line" data-height="30" data-width="120">[content]</div>`;
const secondShape = `<div class="figure figure--L" data-height="90" data-width="60">[content]</div>`;
const thirdShape = `<div class="figure figure--square" data-height="60" data-width="60">[content]</div>`;
const fourthShape = `<div class="figure figure--pattern" data-height="90" data-width="60">[content]</div>`;
const fifthShape = `<div class="figure figure--T" data-height="60" data-width="90">[content]</div>`;
const shapeContent = `<div class="square"></div><div class="square"></div><div class="square"></div><div class="square"></div>`;
const shapeArray = [firstShape, secondShape, thirdShape, fourthShape, fifthShape].map(el => el.replace('[content]', shapeContent));
const leftPositions = [0, 30, 60, 90, 120, 150, 180, 210, 240, 270];

let timeOut = null;
let elements = [];
let x = 0;
let y = -30;
let figureEl = null;
let containerBCR = null;
let figureDetails = null;

const createGrid = (gridContainer, rows, cols) => {
    for (let i = 0; i < rows; i++) {
        const row = document.createElement('div');
        row.classList.add('row');

        for (let j = 0; j < cols; j++) {
            row.innerHTML += `<div class="square"></div>`;
        }

        container.appendChild(row);
    }

    containerBCR = container.getBoundingClientRect();
};

const addRandomFigure = () => {
    if (y === -30) {
        x = 0;
        container.innerHTML += shapeArray[Math.randomFromRange(0, shapeArray.length - 1)];
        const squares = fx(document.querySelectorAll('.figure--stopped > .square'));
        let arrayTenElements = [];

        squares.forEach(square => {
            const fKey = arrayTenElements.find(el => el.key === square.elTop);
            if (!fKey) {
                const obj = {};
                obj.elements = [square.el];
                obj.key = square.elTop;
                arrayTenElements.push(obj);
            } else {
                fKey.elements.push(square.el);
            }
        });

        arrayTenElements.filter(el => el.elements.length === 10).forEach(el => {
            el.elements.forEach(square => {
                if (container.contains(square.parentElement)) {
                    container.removeChild(square.parentElement);
                }
            });
        });

        figureEl = document.querySelector('.figure:last-child');
        figureDetails = {
            width: parseInt(figureEl.getAttribute('data-width'), 10),
            height: parseInt(figureEl.getAttribute('data-height'), 10)
        };
        const filteredLeftPositions = leftPositions.filter(left => left <= container.offsetWidth - figureDetails.width);
        figureEl.style.left = filteredLeftPositions[Math.randomFromRange(0, filteredLeftPositions.length - 1)] + 'px';
        figureEl.style.transform = 'translate(0, 0)';
    }

    if (checkElementYPos()) {
        timeOut = setTimeout(() => requestAnimationFrame(addRandomFigure), 1000);
    } else {
        y = -30;
        figureEl.classList.add('figure--stopped');
        elements.push(figureEl);
        if (Array.from(document.querySelectorAll('.figure--stopped')).some(el => el.getBoundingClientRect().top < 200)) {
            if (timeOut) {
                clearTimeout(timeOut);
                play.classList.add('play-active');
            }
        } else {
            addRandomFigure();
        }
    }
};

const fetchKeyPress = () => {
    const arrowClicked = (pixelsAmount) => {
        const { left, transform } = figureEl.style;
        const elLeft = getElLeft(left, transform);
        const squares = fx(document.querySelectorAll('.figure--stopped > .square'));
        const currentFigureSquares = fx(figureEl.querySelectorAll('.square'), 30).map(({ elLeft, elRight, elTop }) => {
            return {
                elLeft: elLeft + pixelsAmount,
                elRight: elRight + pixelsAmount,
                elTop
            };
        });

        if (pixelsAmount === 30 && elLeft + figureDetails.width < 300) {
            if (!checkSquare(squares, currentFigureSquares)) {
                transformEl(x += 30, y);
            }
        } else if (pixelsAmount === -30 && elLeft > 0) {
            if (!checkSquare(squares, currentFigureSquares)) {
                transformEl(x -= 30, y);
            }
        }
    };

    document.addEventListener('keyup', ({  code }) => {
        if (code === 'ArrowLeft' || code === 'ArrowRight') {
            arrowClicked(code === 'ArrowRight' ? 30 : -30);
        }
    });

    document.addEventListener('keydown', ({  code }) => {
        if (code === 'ArrowDown') {
            checkElementYPos();
        }
    });
};

const transformEl = (xPos , yPos) => figureEl.style.transform = `translate(${ xPos }px, ${ yPos }px)`;

const checkElementYPos = () => {
    const { transform } = figureEl.style;
    let transformYValues = [];
    let isOddFigure = false;

    isOddFigure = figureEl.classList.contains('figure--T') || figureEl.classList.contains('figure--pattern');

    if (elements.length > 0) {
        const currentFigureDetails = {
            elWidth: parseInt(figureEl.getAttribute('data-width'), 10),
            elLeft: Math.floor(figureEl.getBoundingClientRect().left) - Math.floor(containerBCR.left),
            elRight: Math.floor(figureEl.getBoundingClientRect().left) - Math.floor(containerBCR.left) + parseInt(figureEl.getAttribute('data-width'), 10)
        };

        const fCollideElements = Array.from(document.querySelectorAll('.figure--stopped > .square')).filter(el => {
            const squarePosition = {
                elLeft: Math.floor(el.getBoundingClientRect().left) - Math.floor(containerBCR.left),
                elRight: Math.floor(el.getBoundingClientRect().left) - Math.floor(containerBCR.left) + 30
            };

            if (squarePosition.elLeft >= currentFigureDetails.elLeft && squarePosition.elRight <= currentFigureDetails.elRight) {
                return el;
            }
        });

        if (isOddFigure) {
            const squares = fx(document.querySelectorAll('.figure--stopped > .square'));
            const currentFigureSquares = fx(figureEl.querySelectorAll('.square'), 30);

            if (checkSquare(squares, currentFigureSquares)) {
                transformYValues = fCollideElements.map(el => Math.floor(el.getBoundingClientRect().top) - Math.floor(containerBCR.top) - 30);

            } else {
                transformYValues = fCollideElements.map(el => Math.floor(el.getBoundingClientRect().top) - Math.floor(containerBCR.top));
            }
        } else {
            transformYValues = fCollideElements.map(el => Math.floor(el.getBoundingClientRect().top) - Math.floor(containerBCR.top) - 30);
        }
    }

    return getTransformValue(transform, 'y') + figureDetails.height <= (transformYValues.length > 0 ? Math.min(...transformYValues) : Math.floor(containerBCR.height - 30)) ?
        !!transformEl(x , y += 30) :
        false;
};

const getTransformValue = (transform, axe) => parseInt(transform.replace(/[ ()\px|translate\b]/g, '').split(',')[axe === 'x' ? 0 : 1], 10);
const getElLeft = (left, transform) => parseInt(left, 10) + getTransformValue(transform, 'x');
const fx = (elements, valueNumber = 0) => {
    return Array.from(elements).map(el => {
        const { left, top } = el.getBoundingClientRect();
        return {
            elLeft: Math.floor(left) - Math.floor(containerBCR.left),
            elRight: Math.floor(left) - Math.floor(containerBCR.left) + 30,
            elTop: Math.floor(top) - Math.floor(containerBCR.top) + valueNumber,
            el
        };
    });
};
const checkSquare = (squares, currentFigureSquares) => squares.find(el => currentFigureSquares.find(currentFigure => el.elLeft === currentFigure.elLeft && el.elRight === currentFigure.elRight && el.elTop === currentFigure.elTop));
const resetGame = () => {
    btn.addEventListener('click', () => {
        timeOut = null;
        elements = [];
        x = 0;
        y = -30;
        figureEl = null;
        containerBCR = null;
        figureDetails = null;
        container.innerHTML = '';
        createGrid(container, rowsCount, colsCount);
        play.classList.remove('play-active');
        addRandomFigure();
    });
};
createGrid(container, rowsCount, colsCount);
addRandomFigure();
fetchKeyPress();
resetGame();